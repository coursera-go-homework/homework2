package main

import (
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// сюда писать код

func worker(wg *sync.WaitGroup, in, out chan interface{}, j job) {
	defer wg.Done()
	defer close(out)
	j(in, out)
	runtime.Gosched()
}

func ExecutePipeline(jobs ...job) {
	out := make(chan interface{})
	out2 := out
	wg := &sync.WaitGroup{}
	for _, j := range jobs {
		wg.Add(1)
		go worker(wg, out2, out, j)
		out2 = out
		out = make(chan interface{})
	}
	wg.Wait()
}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}

	for v := range in {

		vStr := strconv.Itoa(v.(int))
		wg.Add(1)
		go func(out chan interface{}, vStr string, wg *sync.WaitGroup, mu *sync.Mutex) {
			defer wg.Done()
			wg2 := &sync.WaitGroup{}

			mu.Lock()
			mdHash := DataSignerMd5(vStr)
			mu.Unlock()

			vStrChanOut := make(chan string, 1)
			wg2.Add(1)
			go func(in string, outChan chan string, wg2 *sync.WaitGroup) {
				defer wg2.Done()
				outChan <- DataSignerCrc32(in)
			}(vStr, vStrChanOut, wg2)

			mdChanOut := make(chan string, 1)
			wg2.Add(1)
			go func(in string, outChan chan string, wg2 *sync.WaitGroup) {
				defer wg2.Done()
				outChan <- DataSignerCrc32(in)
			}(mdHash, mdChanOut, wg2)

			wg2.Wait()
			hash := <-vStrChanOut + "~" + <-mdChanOut
			out <- hash
		}(out, vStr, wg, mu)
	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {

	wg := &sync.WaitGroup{}
	for v := range in {
		vStr := v.(string)

		wg.Add(1)

		go func(v string, out chan interface{}, wg *sync.WaitGroup) {
			var concatOutHash string
			defer wg.Done()
			wg2 := &sync.WaitGroup{}
			vStrChanOut := make(chan map[int]string, 6)
			for i := 0; i <= 5; i++ {
				wg2.Add(1)
				go func(in1 int, in2 string, outChan chan map[int]string, wg2 *sync.WaitGroup) {
					defer wg2.Done()
					a := make(map[int]string)
					a[0] = strconv.Itoa(in1)
					a[1] = DataSignerCrc32(strconv.Itoa(in1) + in2)
					outChan <- a
				}(i, v, vStrChanOut, wg2)

			}

			wg2.Wait()
			close(vStrChanOut)

			input := make(map[string]string)
			for vv := range vStrChanOut {
				input[vv[0]] = vv[1]
			}

			keys := make([]string, 0, len(input))
			for k := range input {
				keys = append(keys, k)
			}
			sort.Strings(keys)

			for _, k := range keys {
				concatOutHash = concatOutHash + input[k]
			}

			out <- concatOutHash

		}(vStr, out, wg)

	}
	wg.Wait()

}

func CombineResults(in, out chan interface{}) {
	var input []string
	for v := range in {
		input = append(input, v.(string))
	}
	sort.Strings(input)

	out <- strings.Join(input, "_")
}
